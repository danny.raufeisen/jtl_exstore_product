<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace Plugin\jtl_exstore_product\Portlets\Product;

use Illuminate\Support\Collection;
use JTL\Catalog\Product\Artikel;
use JTL\DB\ReturnType;
use JTL\Filter\AbstractFilter;
use JTL\Filter\Config;
use JTL\Filter\ProductFilter;
use JTL\Filter\Type;
use JTL\OPC\InputType;
use JTL\OPC\Portlet;
use JTL\OPC\PortletInstance;
use JTL\Shop;

/**
 * Class Product
 * @package JTL\OPC\Portlets\Product
 */
class Product extends Portlet
{
    /**
     * @return array
     */
    public function getPropertyDesc(): array
    {
        return [
            'search' => [
                'type'  => InputType::SEARCH,
                'label' => 'Suche',
                'placeholder' => __('search'),
                'width' => 67,
                'order' => 2,
            ],
            'listStyle'    => [
                'type'    => InputType::SELECT,
                'label'   => __('presentation'),
                'width'   => 34,
                'order'   => 1,
                'options' => [
                    'gallery'      => __('presentationGallery'),
                    'list'         => __('presentationList'),
                    'simpleSlider' => __('presentationSimpleSlider'),
                    'slider'       => __('presentationSlider'),
                ],
                'default' => 'gallery',
            ],
            'filter-type'  => [
                'type'     => InputType::RADIO,
                'label'    => 'Einzelne Artikel?',
                'default'  => 'filter',
                'options'  => [
                    'filter' => 'Filter',
                    'singleIDs'    => 'Artikel-IDs',
                ],
                'childrenFor' => [
                    'filter' => [
                        'filters'      => [
                            'type'     => InputType::FILTER,
                            'label'    => __('itemFilter'),
                            'default'  => [],
                            'searcher' => 'search',
                        ],
                    ],
                    'singleIDs' => [
                        'product-ids' => [
                            'type'  => InputType::TEXT,
                            'label' => 'Artikel-Nummern (kommagetrennt)',
                            'default' => '',
                        ]
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getPropertyTabs(): array
    {
        return [
            __('Styles') => 'styles',
        ];
    }

    /**
     * @param PortletInstance $instance
     * @return Collection|array
     */
    public function getFilteredProductIds(PortletInstance $instance)
    {
        $db         = Shop::Container()->getDB();
        $filterType = $instance->getProperty('filter-type');

        if ($filterType === 'singleIDs') {
            $prodIds = explode(',', $instance->getProperty('product-ids'));

            foreach ($prodIds as $i => $prodId) {
                $prodIds[$i] = "'" . $db->escape($prodId) . "'";
            }
            
            $prodIds = implode(',', $prodIds);

            $res = $db->query(
                'SELECT kArtikel FROM tartikel WHERE cArtNr in (' . $prodIds . ')',
                ReturnType::ARRAY_OF_OBJECTS
            );
            return \array_map(
                static function ($obj) {
                    return (int)$obj->kArtikel;
                },
                $res
            );
        }

        $enabledFilters = $instance->getProperty('filters');
        $productFilter  = new ProductFilter(
            Config::getDefault(),
            Shop::Container()->getDB(),
            Shop::Container()->getCache()
        );

        foreach ($enabledFilters as $enabledFilter) {
            /** @var AbstractFilter $newFilter * */
            $newFilter = new $enabledFilter['class']($productFilter);
            $newFilter->setType(Type::AND);
            $productFilter->addActiveFilter($newFilter, $enabledFilter['value']);
        }

        return $productFilter->getProductKeys();
    }

    /**
     * @param PortletInstance $instance
     * @return Artikel[]
     */
    public function getFilteredProducts(PortletInstance $instance): array
    {
        $products = [];
        $options  = Artikel::getDefaultOptions();

        foreach ($this->getFilteredProductIds($instance) as $productID) {
            $product = new Artikel();
            $product->fuelleArtikel($productID, $options);
            $products[] = $product;
        }

        return $products;
    }
}
